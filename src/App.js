import Random from './component/Random.js'
import Turkey from './component/Turkey.js'
import './style.css';
import Germany from './component/Germany.js';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
      

export default function App() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark mb-5 position-fixed w-100">
          <Link className="navbar-brand font-weight-bold">USER</Link>
          <div className="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <Link className="nav-link text-white" to="/">Home</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-white" to="/random">Random</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-white" to="/turkey">Turkey</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-white" to="/germany">Germany</Link>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
              <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/random">
            <Random />
          </Route>
          <Route path="/turkey">
            <Turkey/>
          </Route>
          <Route path="/germany">
            <Germany />
          </Route>
          <Route path="/">
            <Random />
            <Turkey />
            <Germany />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};