import React, { Component } from "react";

class Random extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: []
    }
  }

  componentDidMount() {
    fetch("https://randomuser.me/api/?results=10")
      .then(res => res.json())
      .then(parsedJSON => parsedJSON.results.map(data => (
        {
          id: `${data.id.name}`,
          firstName: `${data.name.first}`,
          lastName: `${data.name.last}`,
          location: `${data.location.state}, ${data.nat}`,
          thumbnail: `${data.picture.large}`
        }
      )))
      .then(items => this.setState({
        items
      }))
      .catch(error => console.log('parsing data is failed', error))
  }

  render() {
    const { items } = this.state
    return (
      <div className="boxWhite">
        <h2 className="header" style={{fontWeight: "bold", marginBottom: "30px"}}>Random User</h2>
        {
          items.length > 0 ? items.map(item => {
            const { id, firstName, lastName, location, thumbnail } = item
            return (
              <div key={id} className="bgCircle">
                <center><img src={thumbnail} alt={firstName} className="circle" /></center> <br />
                <div className="info">
                  {firstName} {lastName} <br />
                  {location}
                </div>
              </div>
            )
          }) : null
        }
      </div>
    )
  };
};

export default Random;


// Fetch Data Task Menampilan Random User dan Spesifik User per Negara di tiap halaman

// 2. Isi App.js dengan 4 halaman (Random, China, Turkey, Jerman)
// 3. Home.js (diisi dengan data 10 random User dari berbagai negara)
// 4. China (diisi dengan data 10 random User dari negara (zhn))
// 5. Turkey (diisi dengan data 10 random User dari negara (Tur))
// 6. Jerman (diisi dengan data 10 random User dari negara (ger))
// 7. Tiap data User diisi dengan (id, firstName, lastName, location, thumbnail)